import socket
import serial
import sys
import atexit


def socket_serial():
    host = socket.gethostname()
    sock = None
    if host == 'g7':
        import os, pty
        python_executable = '/home/brett/anaconda3/envs/fpvbot/bin/python'
        stream_executable = './server_commands/local_stream.sh'
        ports = [8008, 8009, 8010, 8011]
        ip = '127.0.0.1'
        master, slave = pty.openpty()
        s_name = os.ttyname(slave)
        ser = serial.Serial(s_name)
    else:
        ports = [10008, 10009, 10010, 10011]
        ip = 'jetson.local'
        python_executable = '/usr/bin/python'
        stream_executable = './server_commands/stream.sh'
        ser = serial.Serial(
            port='/dev/ttyTHS1',
            baudrate=115200,
            bytesize=serial.EIGHTBITS,
            parity=serial.PARITY_NONE,
            stopbits=serial.STOPBITS_ONE,
            timeout=1,
            xonxoff=False,
            rtscts=False,
            dsrdtr=False,
            writeTimeout=2
        )

    def exit_handler():
        if ser.isOpen():
            ser.close()
            print("Serial port closed.")
        if sock:
            sock.close()
            print("SERIAL socket closed.")

    atexit.register(exit_handler)

    art_chars = ['$', 'L', 'R']
    end_char = '!'

    for port in ports:
        try:
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            server_address = (ip, port)
            print >>sys.stderr, 'Starting tcp SERIAL socket on %s port %s' % server_address
            sock.bind(server_address)
            break
        except socket.error:
            pass

    while True:
        sock.listen(1)
        # Wait for a connection
        print >>sys.stderr, 'waiting for a connection'
        connection, client_address = sock.accept()

        try:
            print >>sys.stderr, 'connection from', client_address
            message = ""
            while True:
                data = str(connection.recv(1))
                if data == "":
                    break
                if data != "\n":
                    message += data
                else:
                    start_char = message[0]
                    end_char = message[-1]
                    if ((start_char == 'R' or start_char == 'L') and (end_char == "!")):
                        print(start_char, end_char)
                        try:
                            print("Sending: ", message)
                            ser.write(message)
                            #reply = ser.readline()
                            #if reply:
                            #    connection.send(reply)
                            #else:
                            #    connection.send('No response from bot\n')
                            message = ""
                        except:
                            print("Error sending command")
        finally:
            print(message)
            connection.close()

if __name__ == "__main__":
    socket_serial()