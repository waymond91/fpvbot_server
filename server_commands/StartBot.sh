#!/bin/bash
if pgrep -x "openocd" > /dev/null
then
	echo "OpenOCD is already running"
else
	echo "Starting Openocd..."
	./openocd_init.sh &
fi

arm-none-eabi-gdb --command=start_commands.gdb

