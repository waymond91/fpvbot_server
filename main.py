import subprocess
import atexit
import sys
import socket
import time
import json

host = socket.gethostname()
sock = None

if host == 'g7':
    import os, pty
    python_executable = '/home/brett/anaconda3/envs/fpvbot/bin/python'
    stream_executable = './server_commands/local_stream.sh'
    ports = [8002, 8003, 8004, 8005 ]
    ip = '127.0.0.1'
    master, slave = pty.openpty()
    s_name = os.ttyname(slave)
else:
    ports = [10000, 10001, 10002, 10003, 10004]
    ip = 'jetson.local'
    python_executable = '/usr/bin/python'
    stream_executable = './server_commands/stream.sh'



class GstreamerManger:
    instance = None
    start_executable = stream_executable
    pkill_string = 'gst'

    def __init__(self):
        self.instance = self
        self.running = False

    def stop(self):
        subprocess.Popen(['sudo', 'pkill', self.pkill_string])
        self.running = False

    def start(self):
        subprocess.Popen(['sudo', self.start_executable])
        self.running = True

    def start_handler(self):
        if not self.running:
            self.start()
            return('Stream enabled.')
        else:
            return('Stream already running.')

    def stop_handler(self):
        if self.running:
            self.stop()
            return('Stream suspended.')
        else:
            return('Stream already suspended')


class GDBManager:
    instance = None
    stop_executable = './server_commands/StopBot.sh &'
    start_executable = './server_commands/StartBot.sh &'
    gdb_sleep_time = 2

    def __init__(self):
        self.instance = self
        self.running = False
        self.stop()

    # This could hang... update gdb commands to ensure clean exit
    def stop(self):
        subprocess.call(self.stop_executable, shell=True)
        # Why not just check if the PID is still alive?
        time.sleep(self.gdb_sleep_time)
        self.running = False


    # This could hang...
    def start(self):
        subprocess.call(self.start_executable, shell=True)
        time.sleep(self.gdb_sleep_time)
        self.running = True

    def start_handler(self):
        if not self.running:
            self.start()
            return('Bot MCU enabled.')
        else:
            return('Bot MCU already running.')

    def stop_handler(self):
        if self.running:
            self.stop()
            return('Bot MCU suspended.')
        else:
            return('Bot MCU already suspended')


fpvbot = GDBManager()
gstreamer = GstreamerManger()


def get_bot_state(jsonify= True):
    bot_state = {}
    bot_state.update({'bot_mcu' : fpvbot.running })
    bot_state.update({'serial_port': ser.isOpen()})
    bot_state.update({'stream': gstreamer.running})
    if jsonify:
        return(json.dumps(bot_state))
    else:
        return(bot_state)


def exit_handler():

    gstreamer.stop()

    if GDBManager.instance:
        GDBManager.instance.stop()

    if sock:
        sock.close()
        print("COMMAND socket closed.")


atexit.register(exit_handler)
commands = {
    "stream-open": gstreamer.start_handler,
    "stream-close": gstreamer.stop_handler,
    "bot-enable" : fpvbot.start_handler,
    "bot-disable": fpvbot.stop_handler,
    "get-state" : get_bot_state,
}


for port in ports:
    try:
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server_address = (ip, port)
        print >>sys.stderr, 'Starting tcp COMMAND socket on %s port %s' % server_address
        sock.bind(server_address)
        break
    except socket.error:
        print >>sys.stderr, 'Socket failed'
        pass

while True:
    sock.listen(1)
    # Wait for a connection
    print >>sys.stderr, 'waiting for a connection'
    connection, client_address = sock.accept()

    try:
        print >>sys.stderr, 'connection from', client_address
        message = ""
        while True:
            data = str(connection.recv(1))
            if data == "":
                break
            if data != "\n":
                message += data
            else:
                if message not in commands.keys():
                    start_char = message[0]
                    end_char = message[-1]
                    if((start_char == 'R' or start_char=='L')and(end_char=="!")):
                        pass
                        """
                        print(start_char, end_char)
                        try:
                            ser.write(message)
                            reply = ser.readline()
                            if reply:
                                connection.send(reply)
                            else:
                                connection.send('No response from bot\n')
                            message = ""
                        except:
                            print("Error sending command")
                        """
                    else:
                        message = ""
                        reply = "Not a valid command. Valid commands are: "+', '.join(commands.keys())
                        reply += '\n'
                        connection.send(reply)
                else:
                    handler = commands[message]
                    reply = handler()
                    reply += '\n'
                    connection.send(reply)
                message = ""


    finally:
        print(message)
        connection.close()
